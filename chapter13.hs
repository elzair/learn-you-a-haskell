import Data.Monoid
import Control.Applicative
import Control.Monad.Writer
import Control.Monad.State
import System.Random

isBigGang :: Int -> (Bool,String)
isBigGang x = (x > 9, "Compared gang size to 9.")

-- applyLog :: (a,String)-> (a -> (b,String)) -> (b,String)
-- applyLog (x,log) f = let (y,newLog) = f x in (y, log ++ newLog)

-- applyLog :: (a,[c]) -> (a -> (b,[c])) -> (b,[c])
-- applyLog (x,log) f = let (y,newLog) = f x in (y, log ++ newLog)

applyLog :: (Monoid m) => (a,m) -> (a -> (b,m)) -> (b,m)
applyLog (x,log) f = let (y,newLog) = f x in (y, mappend log newLog)

type Food = String  
type Price = Sum Int  
  
addDrink :: Food -> (Food,Price)  
addDrink "beans" = ("milk", Sum 25)  
addDrink "jerky" = ("whiskey", Sum 99)  
addDrink _ = ("beer", Sum 30) 

logNumber :: Int -> Writer [String] Int
logNumber x = writer (x, ["Got number: " ++ show x])

multWithLog :: Writer [String] Int
multWithLog = do
  a <- logNumber 3
  b <- logNumber 5
  tell ["Gonna multiply these two"]
  return (a*b)

gcd' :: Int -> Int -> Writer [String] Int
gcd' a b
  | b == 0 = do
      tell ["Finished with " ++ show a]
      return a
  | otherwise = do
      tell [show a ++ " mod " ++ show b ++ " = " ++ show (mod a b)]
      gcd' b (mod a b)
      
gcdReverse :: Int -> Int -> Writer [String] Int  -- Inefficient
gcdReverse a b  
    | b == 0 = do  
        tell ["Finished with " ++ show a]  
        return a  
    | otherwise = do  
        result <- gcdReverse b (a `mod` b)  
        tell [show a ++ " mod " ++ show b ++ " = " ++ show (a `mod` b)]  
        return result
        
newtype DiffList a = DiffList { getDiffList :: [a] -> [a] }

toDiffList :: [a] -> DiffList a
toDiffList xs = DiffList (xs++)

fromDiffList :: DiffList a -> [a]
fromDiffList (DiffList f) = f []

instance Monoid (DiffList a) where
  mempty = DiffList (\xs -> [] ++ xs)
  mappend (DiffList f) (DiffList g) = DiffList (\xs -> f (g xs))

gcdReverse' :: Int -> Int -> Writer (DiffList String) Int  
gcdReverse' a b  
    | b == 0 = do  
        tell (toDiffList ["Finished with " ++ show a])  
        return a  
    | otherwise = do  
        result <- gcdReverse' b (a `mod` b)  
        tell (toDiffList [show a ++ " mod " ++ show b ++ " = " ++ show (a `mod` b)])  
        return result  

finalCountDown :: Int -> Writer (DiffList String) ()  
finalCountDown 0 = do  
    tell (toDiffList ["0"])  
finalCountDown x = do  
    finalCountDown (x-1)  
    tell (toDiffList [show x])  

finalCountDown' :: Int -> Writer [String] ()
finalCountDown' 0 = do
  tell ["0"]
finalCountDown' x = do
  finalCountDown' (x-1)
  tell [show x]

type Stack = [Int]

pop :: Stack -> (Int,Stack)
pop (x:xs) = (x,xs)

push :: Int -> Stack -> ((),Stack)
push a xs = ((),a:xs)

stackManip :: Stack -> (Int, Stack)
stackManip stack = let
  ((),newStack1) = push 3 stack
  (a ,newStack2) = pop newStack1
  in pop newStack2

stackManip' :: Stack -> (Int, Stack)
stackManip' = do
  push 3
  a <- pop
  pop

popState :: State Stack Int
popState = state $ \(x:xs) -> (x,xs)

pushState :: Int -> State Stack ()
pushState a = state $ \xs -> ((),a:xs)

stackManipState :: State Stack Int
stackManipState = do
  pushState 3
  popState
  popState
  
stackStuff :: State Stack ()
stackStuff = do
  a <- popState
  if a == 5
    then pushState 5
    else do
      pushState 3
      pushState 8

moreStack :: State Stack ()
moreStack = do
  a <- stackManipState
  if a == 100
     then stackStuff
     else return ()

stackyStack :: State Stack ()  
stackyStack = do  
    stackNow <- get  
    if stackNow == [1,2,3]  
        then put [8,3,1]  
        else put [9,2,1]  

randomSt :: (RandomGen g, Random a) => State g a
randomSt = state random
    
threeCoins :: State StdGen (Bool,Bool,Bool)
threeCoins = do
  a <- randomSt
  b <- randomSt
  c <- randomSt
  return (a,b,c)

liftM' :: (Monad m) => (a -> b) -> m a -> m b
liftM' f m = m >>= (\x -> return (f x))

ap' :: (Monad m) => m (a -> b) -> m a -> m b  
ap' mf m = do  
    f <- mf  
    x <- m  
    return (f x)

liftA2' :: (Applicative f) => (a -> b -> c) -> f a -> f b -> f c  
liftA2' f x y = f <$> x <*> y
                
join' :: (Monad m) => m (m a) -> m a
join' mm = do
  m <- mm
  m

keepSmall :: Int -> Writer [String] Bool  
keepSmall x  
    | x < 4 = do  
        tell ["Keeping " ++ show x]  
        return True  
    | otherwise = do  
        tell [show x ++ " is too large, throwing it away"]  
        return False  

powerset :: [a] -> [[a]]
powerset xs = filterM (\x -> [True, False]) xs
